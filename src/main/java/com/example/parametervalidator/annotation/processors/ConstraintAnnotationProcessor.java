package com.example.parametervalidator.annotation.processors;


import com.example.parametervalidator.annotation.constraint.Validate;
import com.example.parametervalidator.entity.AnnotationInterceptorRegistration;
import com.example.parametervalidator.validator.core.util.SpringContextUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义约束注解解析器
 */

public class ConstraintAnnotationProcessor {


    public AnnotationInterceptorRegistration parse(Class clazz) {
        AnnotationInterceptorRegistration annotationInterceptorRegistration = new AnnotationInterceptorRegistration();
        Map<String, Object> fieldsMap = new HashMap<>();
        if (clazz.isAnnotationPresent(Validate.class)) {//有这个子注解。视为约束注解。
            annotationInterceptorRegistration.setAnnotationClazz(clazz);
            Field[] fields = clazz.getFields();
            for (Field field :
                    fields) {
                String type = field.getType().getName();
                String name = field.getName();
                fieldsMap.put(name, type);
            }
        }

        return annotationInterceptorRegistration;
    }

}
