package com.example.parametervalidator.annotation;

import com.example.parametervalidator.validator.confregistry.AnnotationCustomValidateRegistrar;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(AnnotationCustomValidateRegistrar.class)
public @interface EnableCustomRestrainAnnotationValidate {

    @AliasFor("basePackages")
    String[] value() default {};

    @AliasFor("value")
    String[] basePackages() default {};

    Class<?>[] annotationClasses() default {};

}
