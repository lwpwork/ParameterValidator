package com.example.parametervalidator.config;

import com.example.parametervalidator.annotation.EnableCustomRestrainAnnotationValidate;
import com.example.parametervalidator.entity.AnnotationInterceptorRegistration;
import com.example.parametervalidator.interceptor.AnnotationInterceptorRegistry;
import com.example.parametervalidator.validator.confregistry.AnnotationRegistryAble;
import com.example.parametervalidator.validator.core.util.SpringContextUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.util.Assert;

import java.util.*;

public class ParameterValidatorConfigUtils  {

    private static Map<String, Object> CustomValidateConfigMap = new HashMap<>();
    private static Integer index = 0;
    //存储可解析的配置的beanDefinition；
    private static List<BeanDefinition> beanDefinitions = new ArrayList<>();

    //注解注册回调
    private static AnnotationRegistryAble annotationRegistryAble = new AnnotationRegistryAble() {
        @Override
        public List<AnnotationInterceptorRegistration> initRegistryable(AnnotationInterceptorRegistry annotationInterceptorRegistry) {
            return ParameterValidatorConfigUtils.initRegistryable(annotationInterceptorRegistry);
        }
    };


    public static void registrarCustomValidateAnnotations(BeanDefinitionRegistry registry) {
        String[] candidateNames = registry.getBeanDefinitionNames();

        //自定义注解注册器,并携带上注册器。
        AnnotationInterceptorRegistry annotationInterceptorRegistry = new AnnotationInterceptorRegistry(registry);
        //遍历所有的beanName，解析配置文件
        for (String beanName :
                candidateNames) {
            BeanDefinition beanDef = registry.getBeanDefinition(beanName);
            try {
                String className = beanDef.getBeanClassName();
                if (className != null && !className.equals("")) {
                    Class clazz = Class.forName(className);
                    if (clazz.isAnnotationPresent(EnableCustomRestrainAnnotationValidate.class)) {//被这个注解所注解的类。
                        beanDefinitions.add(beanDef);
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        //初始化注册，将注册器穿进去，进行注册
        List<AnnotationInterceptorRegistration> registrations = annotationRegistryAble.initRegistryable(annotationInterceptorRegistry);

        if (registrations != null && registrations.size() > 0) {
            for (AnnotationInterceptorRegistration registration :
                    registrations) {
                registrarCustomValidateAnnotation(registration, registry, (Object) null);
            }
        }




        //AnnotationInterceptorRegistration annotationInterceptorRegistration = new AnnotationInterceptorRegistration();
    }

    private static BeanDefinition registrarCustomValidateAnnotation(AnnotationInterceptorRegistration o, BeanDefinitionRegistry registry, Object source) {
        Class clazz = o.getClass();
        Assert.notNull(registry, "BeanDefinitionRegistry must not be null");
        RootBeanDefinition beanDefinition = new RootBeanDefinition(o.getClass());
        beanDefinition.getPropertyValues().add("fields", o.getFields()).add("annotationClazz", o.getAnnotationClazz());

        registry.registerBeanDefinition(clazz.getTypeName() + index, beanDefinition);
        return beanDefinition;
    }

    /**
     * 注册Annotation到bean中。
     * @param annotationInterceptorRegistry
     */
    public static List<AnnotationInterceptorRegistration> initRegistryable(AnnotationInterceptorRegistry annotationInterceptorRegistry) {
        //配置的扫描的注解类型。
        List<Class> annotationClasses = new ArrayList<>();
        //配置的扫描的包的类型。
        List<String> annotationPackages = new ArrayList<>();
        //解析注解
        for (BeanDefinition beanDefinition :
                beanDefinitions) {
            try {
                String className = beanDefinition.getBeanClassName();
                if (className != null && !className.equals("")) {
                    Class clazz = Class.forName(className);
                    EnableCustomRestrainAnnotationValidate annotationValidate = (EnableCustomRestrainAnnotationValidate) clazz.getAnnotation(EnableCustomRestrainAnnotationValidate.class);
                    String [] packages = annotationValidate.value();
                    Class [] classes = annotationValidate.annotationClasses();
                    annotationClasses.addAll(Arrays.asList(classes));
                    annotationPackages.addAll(Arrays.asList(packages));
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        //注册Annotation
        //if (this.annotationPackage!=null&&)
        if (annotationClasses != null && annotationClasses.size() > 0) {
            annotationInterceptorRegistry.addAnnotationClass(annotationClasses);
        }
        if (annotationPackages != null && annotationPackages.size() > 0) {
            annotationInterceptorRegistry.addPackagePath(annotationPackages);
        }
        return annotationInterceptorRegistry.getAnnotationInterceptorRegistrations();
    }

}
