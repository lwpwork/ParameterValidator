package com.example.parametervalidator.interceptor;

import com.example.parametervalidator.annotation.processors.ConstraintAnnotationProcessor;
import com.example.parametervalidator.entity.AnnotationInterceptorRegistration;
import com.example.parametervalidator.validator.confregistry.AnnotationRegistryAble;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class AnnotationInterceptorRegistry {

    private List<AnnotationInterceptorRegistration> annotationInterceptorRegistrations = new ArrayList<>();

    public List<AnnotationInterceptorRegistration> getAnnotationInterceptorRegistrations() {
        return annotationInterceptorRegistrations;
    }

    public void setAnnotationInterceptorRegistrations(List<AnnotationInterceptorRegistration> annotationInterceptorRegistrations) {
        this.annotationInterceptorRegistrations = annotationInterceptorRegistrations;
    }

    private BeanDefinitionRegistry beanDefinitionRegistry;

    public AnnotationInterceptorRegistry(BeanDefinitionRegistry beanDefinitionRegistry) {
        this.beanDefinitionRegistry = beanDefinitionRegistry;
    }

    public AnnotationInterceptorRegistry() {
    }

    public BeanDefinitionRegistry getBeanDefinitionRegistry() {
        return beanDefinitionRegistry;
    }

    public void setBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) {
        this.beanDefinitionRegistry = beanDefinitionRegistry;
    }

    public static void main(String[] args) {
        String[] aaa = new String[10];

        System.out.println(aaa instanceof String[]);
    }


    public AnnotationInterceptorRegistry addAnnotation(Object ... objects){
        return addAnnotation(Arrays.asList(objects));

    }

    public AnnotationInterceptorRegistry addAnnotation(List<Object> objects) {
        if (objects != null && objects.toArray() instanceof String[]) {
            return addPackagePath((String[]) objects.toArray());
        } else {
            return addAnnotationClass((Class[]) objects.toArray());
        }
    }

    public AnnotationInterceptorRegistry addPackagePath(String ... packagePaths){
        return this.addPackagePath(Arrays.asList(packagePaths));
    }

    public AnnotationInterceptorRegistry addPackagePath( List<String> packagePaths){
        return annotationInterceptorRegistry(packagePaths);
    }

    public AnnotationInterceptorRegistry annotationInterceptorRegistry(List<String>  packagePaths) {
        List<Class> annotationClasses = new ArrayList<>();
        for (String path :
                packagePaths) {

        }
        //TODO 获取注解的class


        return this.addAnnotationClass(annotationClasses);
    }

    public AnnotationInterceptorRegistry invokAnnotationInterceptorRegistry( List<AnnotationInterceptorRegistration> annotationInterceptorRegistrations) {
        this.annotationInterceptorRegistrations.addAll(annotationInterceptorRegistrations);
        return this;
    }


    /**
     * 通过注解class配置
     * @param annotationClasses
     * @return
     */
    public AnnotationInterceptorRegistry addAnnotationClass(Class ... annotationClasses){
        return this.addAnnotationClass(Arrays.asList(annotationClasses));
    }

    public AnnotationInterceptorRegistry addAnnotationClass(List<Class> annotationClasses){
        return this.annotationClassInterceptorRegistry(annotationClasses);
    }

    public AnnotationInterceptorRegistry annotationClassInterceptorRegistry(List<Class> annotationClasses) {
        List<AnnotationInterceptorRegistration> annotationInterceptorRegistrations = new ArrayList<>();
        for (Class clazz :
                annotationClasses) {
            ConstraintAnnotationProcessor processor = new ConstraintAnnotationProcessor();
            AnnotationInterceptorRegistration annotationInterceptorRegistration = processor.parse(clazz);
            if (annotationInterceptorRegistration != null) {
                annotationInterceptorRegistrations.add( annotationInterceptorRegistration);
            }
        }
        return invokAnnotationClassInterceptorRegistry(annotationInterceptorRegistrations);
    }

    private AnnotationInterceptorRegistry invokAnnotationClassInterceptorRegistry(List<AnnotationInterceptorRegistration> annotationInterceptorRegistrations) {
        this.annotationInterceptorRegistrations.addAll(annotationInterceptorRegistrations);
        return this;
    }


}
