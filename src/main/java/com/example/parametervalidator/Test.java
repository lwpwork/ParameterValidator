package com.example.parametervalidator;


import com.example.parametervalidator.annotation.EnableCustomRestrainAnnotationValidate;
import com.example.parametervalidator.annotation.constraint.NotNull;
import com.qianbei.rsautil.util.RSAUtils;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
@EnableCustomRestrainAnnotationValidate(annotationClasses = NotNull.class)
public class Test {

    public static void main(String[] args) {
        System.out.println(RSAUtils.generatePrivateKey("12312321"));
    }

}
