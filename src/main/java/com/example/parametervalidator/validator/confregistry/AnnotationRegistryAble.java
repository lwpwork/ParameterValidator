package com.example.parametervalidator.validator.confregistry;

import com.example.parametervalidator.entity.AnnotationInterceptorRegistration;
import com.example.parametervalidator.interceptor.AnnotationInterceptorRegistry;

import java.util.List;

public interface AnnotationRegistryAble {

    List<AnnotationInterceptorRegistration> initRegistryable(AnnotationInterceptorRegistry annotationInterceptorRegistry);


}
