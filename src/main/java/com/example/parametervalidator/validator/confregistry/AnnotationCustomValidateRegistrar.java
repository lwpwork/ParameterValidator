package com.example.parametervalidator.validator.confregistry;

import com.example.parametervalidator.annotation.EnableCustomRestrainAnnotationValidate;
import com.example.parametervalidator.config.ParameterValidatorConfigUtils;
import com.example.parametervalidator.entity.AnnotationInterceptorRegistration;
import com.example.parametervalidator.interceptor.AnnotationInterceptorRegistry;
import com.example.parametervalidator.validator.core.util.SpringContextUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AnnotationCustomValidateRegistrar implements ImportBeanDefinitionRegistrar {

    //配置类的bean定义
    private List<BeanDefinition> beanDefinitions = new ArrayList<>();

    private List<Class> annotationClasses = new ArrayList<>();

    private List<String> annotationPackage = new ArrayList<>();

    @Override
    public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry beanDefinitionRegistry) {

        //注册 自定义注解
        ParameterValidatorConfigUtils.registrarCustomValidateAnnotations(beanDefinitionRegistry);


    }


}
