package com.example.parametervalidator.validator.core.processor;

import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Component
public class ValidAnnotationProcessor {

    public Map<Object,Class> getParamteClass(Method method, Object[] targs){
        //获取参数注解
        Annotation[][] an = method.getParameterAnnotations();

        Map<Object, Class> res = new HashMap<>();
        List<Class> classes = new LinkedList<>();
        int index = 0;
        for (Annotation[] an1 : an) {
            for (Annotation an2 : an1) {
                String type = an2.annotationType().getSimpleName();
                if ("Valid".equals(type)) {//这个参数是被@Valid注解的。
                    Object object = targs[index];
                    //classes.add(objects.getClass());
                    res.put(targs[index], object.getClass());
                }
            }
            index ++;
        }
        return res;
    }

}
