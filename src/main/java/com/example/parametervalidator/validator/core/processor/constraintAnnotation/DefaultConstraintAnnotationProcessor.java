package com.example.parametervalidator.validator.core.processor.constraintAnnotation;

import com.example.parametervalidator.entity.ParameterBean;
import com.example.parametervalidator.validator.core.processor.AbstractAnnotationProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

@Component
public class DefaultConstraintAnnotationProcessor extends AbstractAnnotationProcessor {
    @Override
    public void analysisAnnotation(Object target, Field field,ParameterBean parameterBean) throws Exception {}

    /**
     * 默认启动责任链模式的解析注解。
     * @param clazz
     * @param target
     * @return
     * @throws Exception
     */
    public ParameterBean defaultAnalysisAnnotation(Class clazz, Object target) {
        ParameterBean parameterBean = this.analysisAnnotation(clazz, target);
        return parameterBean;
    }
}
