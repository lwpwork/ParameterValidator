package com.example.parametervalidator.validator.core.processor;

import com.example.parametervalidator.entity.ParameterBean;
import com.example.parametervalidator.validator.result.impl.DeFaultParameterValidatorResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public abstract class ParameterBeanConstraintProcessor {

    @Autowired
    private List<ParameterBeanConstraintProcessor> parameterBeanConstraintProcessors;

    private int index;


    /**
     * 判断是否存在下一个节点。
     * @return
     */
    private boolean hasNext(){
        if (index < parameterBeanConstraintProcessors.size() - 1) {//还有下一个
            index++;
            return true;//返回true，告诉子类，拥有下一个。
        } else {//没有下一个了。那么index应该归为虚无。-1
            index = 0;
            return false;
        }
    }

    /**
     * 校验该注解是否被标识
     *
     * @param parameterBean
     */
    public DeFaultParameterValidatorResult validConstraint(ParameterBean parameterBean, Class clazz) {

        //参数验证
        if (parameterBean!= null && clazz != null) {
            //if (parameterBean.)
        } else {
            if (parameterBean == null) {
                throw new IllegalStateException();
            } else {
                throw new IllegalArgumentException();
            }
        }

        //遍历所有调用链节点
        for (ParameterBeanConstraintProcessor parameterBeanConstraintProcessor :
                parameterBeanConstraintProcessors) {
            parameterBeanConstraintProcessor.validAnnotationConstraint();
        }
        return null;
    }

    /**
     * 验证注解约束条件。
     */
    protected abstract void validAnnotationConstraint();







}
