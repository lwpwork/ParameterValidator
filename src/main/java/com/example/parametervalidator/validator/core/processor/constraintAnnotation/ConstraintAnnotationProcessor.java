package com.example.parametervalidator.validator.core.processor.constraintAnnotation;

import com.example.parametervalidator.annotation.constraint.Null;
import com.example.parametervalidator.entity.AnnotationInterceptorRegistration;
import com.example.parametervalidator.entity.ParameterBean;
import com.example.parametervalidator.entity.ParameterFildeBean;
import com.example.parametervalidator.validator.core.processor.AbstractAnnotationProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.security.auth.login.Configuration;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 解析@Null注解处理器。
 */
@Component
public class ConstraintAnnotationProcessor extends AbstractAnnotationProcessor {

    @Autowired
    private List<AnnotationInterceptorRegistration> annotationInterceptorRegistrations;


    /**
     * 名字起得有点不对，将参数封装成ParameterBean
     * @param target
     * @param field
     * @param parameterBean
     * @throws Exception
     */
    @Override
    public void analysisAnnotation(Object target, Field field, ParameterBean parameterBean) throws Exception {

        //遍历约束注解。
        if (annotationInterceptorRegistrations != null && annotationInterceptorRegistrations.size() > 0) {
            for (AnnotationInterceptorRegistration restrainAnnotation :
                    annotationInterceptorRegistrations) {
                Annotation annotation = field.getAnnotation(restrainAnnotation.getAnnotationClazz());
                if (annotation != null) {//说明属性标注了注解。
                    //解析字段
                    String fieldName = field.getName();
                    String type = field.getType().getTypeName();
                    Method method ;
                    Object value ;
                    if (type.equals("boolean")) {// 基本变量
                        method = target.getClass().getMethod(getBooleanPrefix(fieldName));
                    } else {
                        method = target.getClass().getMethod("get" + getMethodName(fieldName));
                    }
                    value = method.invoke(target);
                    //String annotationType = getAnnotationType(restrainAnnotation.getAnnotationClazz().getTypeName());
                    //屡一下参数。 value有了，type有了，@Null有了。
                    //TODO 构造paramBean
                    Map<Object,List<ParameterFildeBean>> parameter = parameterBean.getParams();
                    List<ParameterFildeBean> params;
                    if (parameter.containsKey(restrainAnnotation.getAnnotationClazz())) {//存在这个约束
                        params = parameter.get(restrainAnnotation.getAnnotationClazz());
                        params.add(new ParameterFildeBean(fieldName, type, value,annotation));
                    } else {//不存"在这个约束
                        params = new ArrayList<>();
                        params.add(new ParameterFildeBean(fieldName,type,value,annotation));
                        parameter.put(restrainAnnotation.getAnnotationClazz(), params);
                    }
                }
            }
        }

    }


}
