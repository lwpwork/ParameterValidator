package com.example.parametervalidator.validator.core.processor;


import com.example.parametervalidator.entity.ParameterBean;

import java.lang.reflect.Field;

/**
 * 解析
 * @param <T>
 */
public interface AnnotationProcessor<T> {

    /**
     * 解析注解
     * @param target
     * @param field
     */
    void analysisAnnotation(Object target, Field field, ParameterBean parameterBean) throws  Exception;

}
