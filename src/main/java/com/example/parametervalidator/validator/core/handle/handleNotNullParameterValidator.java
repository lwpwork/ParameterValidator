package com.example.parametervalidator.validator.core.handle;

import com.example.parametervalidator.annotation.constraint.NotNull;
import com.example.parametervalidator.entity.ParameterBean;
import com.example.parametervalidator.entity.ParameterFildeBean;
import com.example.parametervalidator.validator.core.IParameterValidator;
import com.example.parametervalidator.validator.result.ParameterValidatorResult;
import com.example.parametervalidator.validator.result.entity.ErrorDesc;
import com.example.parametervalidator.validator.result.impl.DeFaultParameterValidatorResult;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 *
 */
@Component
public class handleNotNullParameterValidator implements IParameterValidator {
    @Override
    public ParameterValidatorResult handleParameterValidator(ParameterBean parameterBean, ParameterValidatorResult parameterValidatorResult) {
        Map<Object, List<ParameterFildeBean>> parames =  parameterBean.getParams();
        List<ParameterFildeBean> parameterFildeBeans = parames.get(NotNull.class);
        DeFaultParameterValidatorResult deFaultParameterValidatorResult = (DeFaultParameterValidatorResult) parameterValidatorResult;
        if (parameterFildeBeans != null && parameterFildeBeans.size() > 0) {
            for (ParameterFildeBean fildeBean :
                    parameterFildeBeans) {
                if (fildeBean.getValue() == null) {//验证未通过
                    ErrorDesc errorDesc = ErrorDesc.instantError(fildeBean, "Parameter " + fildeBean.getName() + " cannot be null", -1);
                    deFaultParameterValidatorResult.addErrorDesc(errorDesc);
                }
            }
        }
        return deFaultParameterValidatorResult;
    }
}
