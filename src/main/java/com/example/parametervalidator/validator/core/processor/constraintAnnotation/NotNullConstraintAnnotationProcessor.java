package com.example.parametervalidator.validator.core.processor.constraintAnnotation;

import com.example.parametervalidator.entity.ParameterBean;
import com.example.parametervalidator.validator.core.processor.AbstractAnnotationProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

@Component
public class NotNullConstraintAnnotationProcessor extends AbstractAnnotationProcessor {


    @Override
    public void analysisAnnotation(Object target, Field field, ParameterBean parameterBean) throws Exception {

    }
}
