package com.example.parametervalidator.validator.core;

import com.example.parametervalidator.entity.ParameterBean;
import com.example.parametervalidator.validator.result.ParameterValidatorResult;
import com.example.parametervalidator.validator.result.impl.DeFaultParameterValidatorResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Component
public class DefaultParameterValidator implements IParameterValidator {

    @Autowired
    private List<IParameterValidator> parameterValidators ;

    @Override
    public ParameterValidatorResult handleParameterValidator(ParameterBean parameterBean ,ParameterValidatorResult parameterValidatorResult) {
        return null;
    }


    /**
     * 处理参数
     * @param parameterBean
     * @return
     */
    public ParameterValidatorResult validateParameter(ParameterBean parameterBean){
        ParameterValidatorResult result = new DeFaultParameterValidatorResult();
        if (!parameterBean.isVerify()) {
            //校验参数
            if (parameterValidators != null) {
                Iterator<IParameterValidator> iterable =  parameterValidators.iterator();
                while (iterable.hasNext()) {
                    IParameterValidator parameterValidator = iterable.next();
                    parameterValidator.handleParameterValidator(parameterBean,result);
                }
            }

        }
        return result;
    }


    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("111", "111");
         test(map, "222");
         test(map, "333");
        System.out.println(map);

    }

    public static Map<String,String> test(Map<String, String> map,String name){
        map.put(name, name);
        return map;
    }
}
