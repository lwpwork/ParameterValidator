package com.example.parametervalidator.validator.core;


import com.example.parametervalidator.entity.ParameterBean;
import com.example.parametervalidator.validator.result.ParameterValidatorResult;

/**
 *
 *  参数校验接口
 */
public interface IParameterValidator {


    /**
     * 解析参数并验证。
     * @param parameterBean 参数解析
     * @return
     */
    ParameterValidatorResult handleParameterValidator(ParameterBean parameterBean,ParameterValidatorResult parameterValidatorResult);


}
