package com.example.parametervalidator.validator.result.impl;

import com.example.parametervalidator.entity.ParameterBean;
import com.example.parametervalidator.validator.result.ParameterValidatorResult;
import com.example.parametervalidator.validator.result.entity.ErrorDesc;

import java.util.ArrayList;
import java.util.List;

public class DeFaultParameterValidatorResult extends AbstractParameterValidatorResult {


    @Override
    public List<ErrorDesc> errorDesc() {
        return this.errorDescs;
    }

    @Override
    public boolean hasInvalidParameter() {
        if (this.errorDescs != null && this.errorDescs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }


    public void addErrorDesc(ErrorDesc error){
        if (this.errorDescs == null) {
            this.errorDescs = new ArrayList<>();
        }
        this.errorDescs.add(error);
    }

    public void addAllErrorDescs(List<ErrorDesc> errorDescs){
        if (this.errorDescs == null) {
            this.errorDescs = new ArrayList<>();
        }
        this.errorDescs.addAll(errorDescs);
    }




}
