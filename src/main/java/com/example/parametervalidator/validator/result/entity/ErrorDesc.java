package com.example.parametervalidator.validator.result.entity;

import com.example.parametervalidator.entity.ParameterFildeBean;
import lombok.Data;

/**
 * 参数Error说明
 */
@Data
public class ErrorDesc {

    private String msg;

    private Integer code;

    /**
     * 自定义异常
     */
    private Exception errorExc;

    /**
     * 是否抛出自定义异常
     */
    private boolean throwExc = false;

    /**
     * 参数名
     */
    private String name;

    /**
     * 参数类型
     */
    private String type;


    public ErrorDesc(String msg, Integer code, String name, String type) {
        this.msg = msg;
        this.code = code;
        this.name = name;
        this.type = type;
    }

    public ErrorDesc() {
    }

    public ErrorDesc(String msg, Integer code) {
        this.msg = msg;
        this.code = code;
    }

    public ErrorDesc(String msg, Integer code, Exception errorExc, boolean throwExc) {
        this.msg = msg;
        this.code = code;
        this.errorExc = errorExc;
        this.throwExc = throwExc;
    }

    public ErrorDesc(String msg, Integer code, Exception errorExc, boolean throwExc, String name, String type) {
        this.msg = msg;
        this.code = code;
        this.errorExc = errorExc;
        this.throwExc = throwExc;
        this.name = name;
        this.type = type;
    }

    public static ErrorDesc instantError(ParameterFildeBean parameterFildeBean, String msg, Integer code){
        return new ErrorDesc(msg, code, parameterFildeBean.getName(), parameterFildeBean.getType());
    }

    public static ErrorDesc instantError(String msg,Integer code){
        return new ErrorDesc(msg, code);
    }

    public static ErrorDesc instantError(String msg,Integer code,Exception e){
        return new ErrorDesc(msg, code, e, true);
    }

    public static ErrorDesc instantError(String msg,Integer code,Exception e,ParameterFildeBean parameterFildeBean){
        return new ErrorDesc(msg, code, e, true,parameterFildeBean.getName(),parameterFildeBean.getType());
    }



}
