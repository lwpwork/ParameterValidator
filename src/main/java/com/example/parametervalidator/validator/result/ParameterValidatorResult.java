package com.example.parametervalidator.validator.result;

import com.example.parametervalidator.entity.ParameterBean;
import com.example.parametervalidator.validator.result.entity.ErrorDesc;

import java.util.List;

public interface ParameterValidatorResult {

    List<ErrorDesc> errorDesc();

    boolean hasInvalidParameter();

}
