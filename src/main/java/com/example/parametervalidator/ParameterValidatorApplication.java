package com.example.parametervalidator;

import com.example.parametervalidator.annotation.EnableCustomRestrainAnnotationValidate;
import com.example.parametervalidator.annotation.constraint.NotNull;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableCustomRestrainAnnotationValidate(annotationClasses = NotNull.class)
public class ParameterValidatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParameterValidatorApplication.class, args);
	}

}
