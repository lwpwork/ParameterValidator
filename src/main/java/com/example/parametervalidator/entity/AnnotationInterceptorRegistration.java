package com.example.parametervalidator.entity;


import lombok.Data;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;

/**
 * 注解配置，将注解解析到该类中。
 *
 */
@Data
public class AnnotationInterceptorRegistration {


    //注解的属性。包括名称和类型
    private Map<String, Object> fields;

    //注解的Clazz
    private Class<Annotation> annotationClazz;

    public AnnotationInterceptorRegistration() {
    }

    public AnnotationInterceptorRegistration(Map<String, Object> fields, Class<Annotation> annotationClazz) {
        this.fields = fields;
        this.annotationClazz = annotationClazz;
    }

    public AnnotationInterceptorRegistration(Map<String, Object> fields) {
        this.fields = fields;
    }

    public AnnotationInterceptorRegistration(Class<Annotation> annotationClazz) {
        this.annotationClazz = annotationClazz;
    }
}
