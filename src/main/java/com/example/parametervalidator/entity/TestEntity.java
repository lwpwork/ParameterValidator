package com.example.parametervalidator.entity;

import com.example.parametervalidator.annotation.constraint.NotNull;
import com.example.parametervalidator.annotation.constraint.Null;
import lombok.Data;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

@Data
public class TestEntity {

    @NotNull
    private String name;

    @NotNull
    private Integer age;

    @NotNull
    private String motto;

    @NotNull
    private Boolean isenable;

    public TestEntity(String name, Integer age, String motto) {
        this.name = name;
        this.age = age;
        this.motto = motto;
    }

    public TestEntity() {
    }



}
