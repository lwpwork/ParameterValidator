package com.example.parametervalidator.entity;


import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 解析参数实体类，并封装成ParameterBean
 * ParameterBean
 */
@Data
public class ParameterBean {

    //是否校验
    private boolean verify;

    //校验实体的全类名
    private String className;

    //需要校验的参数列表。
    private Map<Object,List<ParameterFildeBean>> params;










}
