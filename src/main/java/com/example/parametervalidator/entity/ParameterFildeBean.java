package com.example.parametervalidator.entity;

import lombok.Data;

import java.lang.annotation.Annotation;

@Data
public class ParameterFildeBean {
    private String name;
    private String type;
    private Object value;
    private Annotation annotation;

    public ParameterFildeBean(String name, String type, Object value,Annotation annotation) {
        this.name = name;
        this.type = type;
        this.value = value;
        this.annotation = annotation;
    }

    public ParameterFildeBean() {
    }
}
